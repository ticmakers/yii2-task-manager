<?php

namespace ticmakers\taskmanager\models\task;

use ticmakers\taskmanager\models\app\ScheduledTaskLogs as STL;
use Yii;
use ticmakers\taskmanager\Module;

class TaskBase implements TaskInterface
{
    const STATUS_SUCCESSFUL = 'SUCCESSFUL';
    const STATUS_FAILED = 'FAILED';

    public function run($id)
    {
        $module = Module::getInstance();
        throw new NotSupportedException(Yii::t($module->id, '"{function}" is not implemented.', ['function' => 'run']));
    }

    /**
     * Permite el registro del comentarios y estados de las tareas programdas
     *
     * @param [type] $taskId
     * @param [type] $result
     * @param [type] $comments
     * @return void
     */
    public function registerLog($taskId, $result, $comments)
    {
        if (Yii::$app instanceof \yii\console\Application) {
            $result = Yii::$app->db->createCommand()
                ->insert(STL::tableName(), [
                    'scheduled_task_id' => $taskId,
                    'result' => $result,
                    'comments' => $comments,
                    'active' => STL::STATUS_ACTIVE,
                    STL::CREATED_AT_COLUMN => STL::DEFAULT_USER_ID,
                    STL::CREATED_DATE_COLUMN => date('Y-m-d H:i:s'),
                    STL::UPDATED_AT_COLUMN => STL::DEFAULT_USER_ID,
                    STL::UPDATED_DATE_COLUMN => date('Y-m-d H:i:s'),
                ])
                ->execute();
        } else {
            $model = new STL();
            $model->scheduled_task_id = $taskId;
            $model->result = $result;
            $model->comments = $comments;
            $result = $model->save();
        }

        return $result;
    }

    /**
     * Undocumented function
     *
     * @param [type] $status
     * @param boolean $getValue
     * @return void
     */
    public static function getResultStatus($status = null, $getValue = false)
    {
        $module = Module::getInstance();
        $statusData = [
            static::STATUS_SUCCESSFUL => Yii::t($module->id, 'Successful'),
            static::STATUS_FAILED =>  Yii::t($module->id, 'Failed'),
        ];

        if (empty($status) && !$getValue) {
            $result = $statusData;
        } else if ($getValue && isset($statusData[$status])) {
            $result = $statusData[$status];
        } else {
            $result = $status;
        }

        return $result;
    }
}
