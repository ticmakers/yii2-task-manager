<?php
namespace ticmakers\taskmanager\models\task;

interface TaskInterface
{
    public function run($id);
}
