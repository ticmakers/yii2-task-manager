<?php

namespace ticmakers\taskmanager\models\app;

use Yii;

/**
 * Éste es el modelo para la tabla "task_periodicities".
 * 
 *
 * @package ticmakers\taskmanager\models\app 
 *
 * @property integer $task_periodicity_id 
 * @property string $type 
 * @property string $value 
 * @property string $description 
 * @property string $selectable 
 * @property string $active 
 * @property integer $created_by 
 * @property string $created_at 
 * @property integer $updated_by 
 * @property string $updated_at 
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TIC Makers S.A.S.
 */
class TaskPeriodicities extends \ticmakers\taskmanager\models\base\TaskPeriodicities
{
    const TYPE_MINUTE = 'MIN';
    const TYPE_HOUR = 'HOU';
    const TYPE_DAY = 'DAY';
    const TYPE_MONTH = 'MON';
    const TYPE_WEEKDAY = 'WEEK';
    const TYPE_COMMON = 'COM';

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [];
        return Yii::$app->arrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [];
        return Yii::$app->arrayHelper::merge(parent::attributeLabels(), $attributeLabels);
    }

    /**
     * @inheritDoc
     */
    public function getHelp($attribute = null)
    {
        $newhelps = [];
        $helps = Yii::$app->arrayHelper::merge(parent::getHelp(), $newhelps);
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * @inheritDoc
     */
    public static function getNameFromRelations()
    {
        return 'description';
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $newBehaviors = [];
        return Yii::$app->arrayHelper::merge(parent::behaviors(), $newBehaviors);
    }

    /**
     * @inheritDoc
     */
    public function formColumns()
    {
        $formColumns = [];
        return Yii::$app->arrayHelper::merge(parent::formColumns(), $formColumns);
    }

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $gridColumns = [];
        return Yii::$app->arrayHelper::merge(parent::gridColumns(), $gridColumns);
    }
}
