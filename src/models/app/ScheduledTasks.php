<?php

namespace ticmakers\taskmanager\models\app;

use kartik\grid\GridView;
use Yii;

/**
 * Éste es el modelo para la tabla "scheduled_tasks".
 * 
 *
 * @package ticmakers\taskmanager\models\app 
 *
 * @property integer $scheduled_task_id Scheduled task identifier
 * @property string $name Scheduled task name
 * @property string $description Description of the scheduled task
 * @property string $script Name of the script which calls the scheduled task.
 * @property string $web Option that should be called when the task is done manually from the web part.
 * @property string $minute Minute when the scheduled task is executed
 * @property string $hour Hour when the scheduled task is executed
 * @property string $day Day when the scheduled task is executed
 * @property string $month Month when the scheduled task is executed
 * @property string $weekday Weekday when the scheduled task is executed
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TIC Makers S.A.S.
 */
class ScheduledTasks extends \ticmakers\taskmanager\models\base\ScheduledTasks
{

    /**
     * @inheritDoc
     */
    public $includeActionColumns = false;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $_lastRun;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $commonConfig;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [];
        return Yii::$app->arrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [
            'periodicity' => Yii::t('taskmanager', 'Periodicity'),
            'lastRun' => Yii::t('taskmanager', 'Last run')
        ];
        return Yii::$app->arrayHelper::merge(parent::attributeLabels(), $attributeLabels);
    }

    /**
     * @inheritDoc
     */
    public function getHelp($attribute = null)
    {
        $newhelps = [];
        $helps = Yii::$app->arrayHelper::merge(parent::getHelp(), $newhelps);
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * @inheritDoc
     */
    public static function getNameFromRelations()
    {
        return parent::getNameFromRelations();
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $newBehaviors = [];
        return Yii::$app->arrayHelper::merge(parent::behaviors(), $newBehaviors);
    }

    /**
     * @inheritDoc
     */
    public function formColumns()
    {
        $formColumns = [];
        return Yii::$app->arrayHelper::merge(parent::formColumns(), $formColumns);
    }

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $gridColumns = [
            'name' => [
                'attribute' => 'name',
            ],
            'periodicity' => [
                'attribute' => 'periodicity',
            ],
            'lastRun' => [
                'attribute' => 'lastRun',
                'value' => function ($model) {
                    return $model->getLastRun(true);
                },
                'format' => 'datetime',
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => ['id' => 'lastRun_grid'],
            ],
            'actions' => [
                'class' => 'ticmakers\core\base\ActionColumn',
                'isModal' => false,
                'dynaModalId' => 'tarea-programada',
                'template' => '{update} {log} {run}',
                'buttons' => [
                    'log' => function ($url, $model) {

                        return Yii::$app->html::a(
                            '<span class="fa fa-list" title="Logs"></span>',
                            $url,
                            [
                                'data-pjax' => false,
                                'onclick' => "$('#default-modal').find('.modal-body').html('');openModalGrid(this, 'default', 'log'); return false;",
                            ]
                        );
                    },
                    'run' => function ($url, $model) {
                        return Yii::$app->html::a('<span class="fa fa-play" title="Ejecutar"></span>', $url);
                    },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'log') {
                        return \yii\helpers\Url::to(['scheduled-task-logs/index', 'scheduled_task_id' => $key]);
                    } else {
                        return \yii\helpers\Url::toRoute([$action, 'id' => $key]);
                    }
                },
            ],
            'script' => [
                'visible' => false
            ],
            'web' => [
                'visible' => false
            ],
            'minute' => [
                'visible' => false
            ],
            'hour' => [
                'visible' => false
            ],
            'day' => [
                'visible' => false
            ],
            'month' => [
                'visible' => false
            ],
            'weekday' => [
                'visible' => false
            ],
            'created_at' => [
                'visible' => false
            ],
            'updated_at' => [
                'visible' => false
            ],
        ];
        return Yii::$app->arrayHelper::merge(parent::gridColumns(), $gridColumns);
    }

    /**
     * @inheritDoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        try {
            if ($insert) {
                Yii::$app->taskManager->appendCronjob($this->cronJob);
            } else {
                Yii::$app->taskManager->removeCronjob($this->cronJob);
                if ($this->{static::STATUS_COLUMN} == static::STATUS_ACTIVE) {
                    Yii::$app->taskManager->appendCronjob($this->cronJob);
                }
            }
        } catch (yii\console\Exception $ex) {
            Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t('taskmanager', 'It is not possible to create the scheduled task on the server.'));
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function setLastRun($lastRun)
    {
        $this->_lastRun = $lastRun;
    }

    public function getLastRun($sourceDb = false)
    {
        if ($sourceDb) {
            $lastRun = Yii::$app
                ->db
                ->createCommand('select max(created_at) from scheduled_task_logs where active=:active AND scheduled_task_id=:task', [
                    ':active' => static::STATUS_ACTIVE,
                    ':task' => $this->scheduled_task_id,
                ])
                ->queryScalar();
        } else {
            $lastRun = $this->_lastRun;
        }

        return $lastRun ?? null;
    }


    public function getCronJob()
    {
        return [
            $this->getPeriodicity() . ' php ' . Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'yii taskmanager/task/run ' . $this->scheduled_task_id,
        ];
    }

    public function getPeriodicity()
    {
        $periodicities = TaskPeriodicities::find()
            ->where(['task_periodicity_id' => [
                $this->day,
                $this->weekday,
                $this->hour,
                $this->month,
                $this->minute,
            ]])
            ->asArray()
            ->all();

        $crontab = '';

        if ($periodicities) {
            $periodicities = \yii\helpers\ArrayHelper::index(
                $periodicities,
                'task_periodicity_id'
            );

            $crontab = $periodicities[$this->minute]['value'] ?? '';
            $crontab .= ' ' . $periodicities[$this->hour]['value'] ?? '';
            $crontab .= ' ' . $periodicities[$this->day]['value'] ?? '';
            $crontab .= ' ' . $periodicities[$this->month]['value'] ?? '';
            $crontab .= ' ' . $periodicities[$this->weekday]['value'] ?? '';
        }

        return $crontab;
    }
}
