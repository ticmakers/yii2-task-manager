<?php

namespace ticmakers\taskmanager\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "scheduled_task_logs".
 * 
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $scheduled_task_log_id Scheduled task log identifier
 * @property integer $scheduled_task_id scheduled task identifier
 * @property string $result It takes a two values:  1. Successful 2. Failed
 * @property string $comments scheduled task log comments
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TIC Makers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class ScheduledTaskLogs extends \ticmakers\core\base\Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'scheduled_task_logs';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['scheduled_task_id', 'result'], 'required'],
            [['scheduled_task_id'], 'integer'],
            [['comments'], 'string'],
            [['result'], 'string', 'max' => 255],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'scheduled_task_log_id' => Yii::t('app', 'Código'),
            'scheduled_task_id' => Yii::t('app', 'Scheduled task'),
            'result' => Yii::t('app', 'Result'),
            'comments' => Yii::t('app', 'Comments'),
            'active' => Yii::t('app', 'Active'),
            'created_by' => Yii::t('app', 'Created by'),
            'created_at' => Yii::t('app', 'Created at'),
            'updated_by' => Yii::t('app', 'Updated by'),
            'updated_at' => Yii::t('app', 'Updated at'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'scheduled_task_log_id' => Yii::t('app', 'Scheduled task log identifier'),
            'scheduled_task_id' => Yii::t('app', 'Scheduled task identifier'),
            'result' => Yii::t('app', 'It takes a two values:  1. successful 2. failed'),
            'comments' => Yii::t('app', 'Scheduled task log comments'),
            'active' => Yii::t('app', 'Define if it\'s active or not. format : y -> yes, n -> no'),
            'created_by' => Yii::t('app', 'It\'s the identifier of the user who created the record.'),
            'created_at' => Yii::t('app', 'Define the creation date and time. format: yyyy-mm-dd hh: mm: ss'),
            'updated_by' => Yii::t('app', 'It\'s the identifier of the user who updated the record.'),
            'updated_at' => Yii::t('app', 'Define the update date and time. format: yyyy-mm-dd hh: mm: ss'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|ScheduledTaskLogs[]
     */
    public static function getData($list = true, $attributes = [], $orderBy = [])
    {
        if (!isset($attributes[static::STATUS_COLUMN])) {
            $attributes[static::STATUS_COLUMN] = static::STATUS_ACTIVE;
        }

        if (empty($orderBy)) {
            $orderBy[static::getNameFromRelations()] = SORT_ASC;
        }

        $query = new \ArrayObject(static::find()->where($attributes)->orderBy($orderBy)->cache(3)->asArray()->all());
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'scheduled_task_log_id', static::getNameFromRelations());
        }
        return $query;
    }
}
