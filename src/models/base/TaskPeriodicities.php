<?php

namespace ticmakers\taskmanager\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "task_periodicities".
 * 
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $task_periodicity_id 
 * @property string $type 
 * @property string $value 
 * @property string $description 
 * @property string $selectable 
 * @property string $active 
 * @property integer $created_by 
 * @property string $created_at 
 * @property integer $updated_by 
 * @property string $updated_at 
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TIC Makers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class TaskPeriodicities extends \ticmakers\core\base\Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'task_periodicities';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['type', 'value', 'selectable'], 'required'],
            [['type', 'value', 'description'], 'string', 'max' => 255],
            [['selectable'], 'string', 'max' => 1],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'task_periodicity_id' => Yii::t('taskmanager', 'Código'),
            'type' => Yii::t('taskmanager', 'Type'),
            'value' => Yii::t('taskmanager', 'Value'),
            'description' => Yii::t('taskmanager', 'Description'),
            'selectable' => Yii::t('taskmanager', 'Selectable'),
            'active' => Yii::t('taskmanager', 'Active'),
            'created_by' => Yii::t('taskmanager', 'Created by'),
            'created_at' => Yii::t('taskmanager', 'Created at'),
            'updated_by' => Yii::t('taskmanager', 'Updated by'),
            'updated_at' => Yii::t('taskmanager', 'Updated at'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'task_periodicity_id' => Yii::t('taskmanager', ''),
            'type' => Yii::t('taskmanager', ''),
            'value' => Yii::t('taskmanager', ''),
            'description' => Yii::t('taskmanager', ''),
            'selectable' => Yii::t('taskmanager', ''),
            'active' => Yii::t('taskmanager', ''),
            'created_by' => Yii::t('taskmanager', ''),
            'created_at' => Yii::t('taskmanager', ''),
            'updated_by' => Yii::t('taskmanager', ''),
            'updated_at' => Yii::t('taskmanager', ''),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|TaskPeriodicities[]
     */
    public static function getData($list = true, $attributes = [], $orderBy = [])
    {
        if (!isset($attributes[static::STATUS_COLUMN])) {
            $attributes[static::STATUS_COLUMN] = static::STATUS_ACTIVE;
        }

        if (empty($orderBy)) {
            $orderBy[static::getNameFromRelations()] = SORT_ASC;
        }

        $query = new \ArrayObject(static::find()->where($attributes)->orderBy($orderBy)->cache(3)->asArray()->all());
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'task_periodicity_id', static::getNameFromRelations());
        }
        return $query;
    }
}
