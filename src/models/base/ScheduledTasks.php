<?php

namespace ticmakers\taskmanager\models\base;

use ticmakers\taskmanager\components\Model;
use Yii;

/**
 * Éste es el modelo para la tabla "scheduled_tasks".
 * 
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $scheduled_task_id Scheduled task identifier
 * @property string $name Scheduled task name
 * @property string $description Description of the scheduled task
 * @property string $script Name of the script which calls the scheduled task.
 * @property string $web Option that should be called when the task is done manually from the web part.
 * @property string $minute Minute when the scheduled task is executed
 * @property string $hour Hour when the scheduled task is executed
 * @property string $day Day when the scheduled task is executed
 * @property string $month Month when the scheduled task is executed
 * @property string $weekday Weekday when the scheduled task is executed
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TIC Makers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class ScheduledTasks extends Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'scheduled_tasks';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'script', 'web', 'minute', 'hour', 'day', 'month', 'weekday'], 'required'],
            [['description'], 'string'],
            [['name', 'script', 'web', 'minute', 'hour', 'day', 'month', 'weekday'], 'string', 'max' => 255],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'scheduled_task_id' => Yii::t('app', 'Código'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'script' => Yii::t('app', 'Script'),
            'web' => Yii::t('app', 'Web'),
            'minute' => Yii::t('app', 'Minute'),
            'hour' => Yii::t('app', 'Hour'),
            'day' => Yii::t('app', 'Day'),
            'month' => Yii::t('app', 'Month'),
            'weekday' => Yii::t('app', 'Weekday'),
            'active' => Yii::t('app', 'Active'),
            'created_by' => Yii::t('app', 'Created by'),
            'created_at' => Yii::t('app', 'Created at'),
            'updated_by' => Yii::t('app', 'Updated by'),
            'updated_at' => Yii::t('app', 'Updated at'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'scheduled_task_id' => Yii::t('app', 'Scheduled task identifier'),
            'name' => Yii::t('app', 'Scheduled task name'),
            'description' => Yii::t('app', 'Description of the scheduled task'),
            'script' => Yii::t('app', 'Name of the script which calls the scheduled task.'),
            'web' => Yii::t('app', 'Option that should be called when the task is done manually from the web part.'),
            'minute' => Yii::t('app', 'Minute when the scheduled task is executed'),
            'hour' => Yii::t('app', 'Hour when the scheduled task is executed'),
            'day' => Yii::t('app', 'Day when the scheduled task is executed'),
            'month' => Yii::t('app', 'Month when the scheduled task is executed'),
            'weekday' => Yii::t('app', 'Weekday when the scheduled task is executed'),
            'active' => Yii::t('app', 'Define if it\'s active or not. format : y -> yes, n -> no'),
            'created_by' => Yii::t('app', 'It\'s the identifier of the user who created the record.'),
            'created_at' => Yii::t('app', 'Define the creation date and time. format: yyyy-mm-dd hh: mm: ss'),
            'updated_by' => Yii::t('app', 'It\'s the identifier of the user who updated the record.'),
            'updated_at' => Yii::t('app', 'Define the update date and time. format: yyyy-mm-dd hh: mm: ss'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|ScheduledTasks[]
     */
    public static function getData($list = true, $attributes = [], $orderBy = [])
    {
        if (!isset($attributes[static::STATUS_COLUMN])) {
            $attributes[static::STATUS_COLUMN] = static::STATUS_ACTIVE;
        }

        if (empty($orderBy)) {
            $orderBy[static::getNameFromRelations()] = SORT_ASC;
        }

        $query = new \ArrayObject(static::find()->where($attributes)->orderBy($orderBy)->cache(3)->asArray()->all());
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'scheduled_task_id', static::getNameFromRelations());
        }
        return $query;
    }
}
