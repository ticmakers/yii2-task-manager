<?php

namespace ticmakers\taskmanager\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use ticmakers\taskmanager\models\app\ScheduledTasks as ScheduledTaskModel;

/**
 * Esta clase representa las búsqueda para el modelo `ticmakers\taskmanager\models\base\ScheduledTask`.
 *
 * @package app
 * @subpackage models/searchs
 * @category Models
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S. 
 * @version 0.0.1
 * @since 1.0.0
 */
class ScheduledTask extends ScheduledTaskModel
{
    /**
     * Define las reglas de validación de los datos.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['scheduled_task_id', 'created_by', 'updated_by'], 'integer'],
            [['name', 'description', 'script', 'web', 'minute', 'hour', 'day', 'month', 'weekday', 'active', 'created_at', 'updated_at', 'lastRun'], 'safe'],
        ];
    }

    /**
     * Escenarios del Modelo
     *
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Crea una instancia de un provider de datos con el query de búsqueda aplicado
     *
     * @param array $params Parametros para la búsqueda
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find();
        $query->alias('task');

        //Agrega condiciones que quieras aplicar siempre aquí

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $sortAttributes = $dataProvider->getSort()->attributes;

        $newSortAttributes = [
            'lastRun' => [
                'asc' => ['(select max(logsort.created_at) from ' . ScheduledTaskLogs::tableName() . ' logsort where task.scheduled_task_id = logsort.scheduled_task_id AND logsort.active =\'Y\')' => SORT_ASC],
                'desc' => ['(select max(logsort.created_at) from ' . ScheduledTaskLogs::tableName() . ' logsort where task.scheduled_task_id = logsort.scheduled_task_id AND logsort.active =\'Y\')' => SORT_DESC]
            ]
        ];

        $dataProvider->setSort([
            'attributes' => array_merge($sortAttributes, $newSortAttributes),
            'defaultOrder' => ['created_at' => SORT_DESC]
        ]);


        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($lastRun = $this->getLastRun(false)) {
            $query->innerJoin(ScheduledTaskLogs::tableName() . ' log', 'task.scheduled_task_id = log.scheduled_task_id AND DATE(log.created_at) =:lastRun AND log.active =:active', [':lastRun' => $lastRun, ':active' => static::STATUS_ACTIVE]);
        }

        //Condición para filtros
        $query->andFilterWhere([
            'task.scheduled_task_id' => $this->scheduled_task_id,
            'task.created_by' => $this->created_by,
            'DATE(task.created_at)' => $this->created_at,
            'task.updated_by' => $this->updated_by,
            'DATE(task.updated_at)' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'task.name', $this->name])
            ->andFilterWhere(['ilike', 'task.description', $this->description])
            ->andFilterWhere(['like', 'task.script', $this->script])
            ->andFilterWhere(['like', 'task.web', $this->web])
            ->andFilterWhere(['like', 'task.minute', $this->minute])
            ->andFilterWhere(['like', 'task.hour', $this->hour])
            ->andFilterWhere(['like', 'task.day', $this->day])
            ->andFilterWhere(['like', 'month', $this->month])
            ->andFilterWhere(['like', 'task.weekday', $this->weekday])
            ->andFilterWhere(['like', 'task.active', $this->active]);

        $dataProvider->sort->defaultOrder = [static::getNameFromRelations() => SORT_ASC];

        return $dataProvider;
    }
}
