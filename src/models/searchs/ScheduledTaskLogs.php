<?php

namespace ticmakers\taskmanager\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use ticmakers\taskmanager\models\app\ScheduledTaskLogs as ScheduledTaskLogsModel;

/**
 * Esta clase representa las búsqueda para el modelo `ticmakers\taskmanager\models\base\ScheduledTaskLogs`.
 *
 * @package app
 * @subpackage models/searchs
 * @category Models
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S. 
 * @version 0.0.1
 * @since 1.0.0
 */
class ScheduledTaskLogs extends ScheduledTaskLogsModel
{
    /**
     * Define las reglas de validación de los datos.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['scheduled_task_log_id', 'scheduled_task_id', 'created_by', 'updated_by'], 'integer'],
            [['result', 'comments', 'active', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * Escenarios del Modelo
     *
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Crea una instancia de un provider de datos con el query de búsqueda aplicado
     *
     * @param array $params Parametros para la búsqueda
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find();

        //Agrega condiciones que quieras aplicar siempre aquí

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        //Condición para filtros
        $query->andFilterWhere([
            'scheduled_task_log_id' => $this->scheduled_task_log_id,
            'scheduled_task_id' => $this->scheduled_task_id,
            'created_by' => $this->created_by,
            'DATE(created_at)' => $this->created_at,
            'updated_by' => $this->updated_by,
            'DATE(updated_at)' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'result', $this->result])
            ->andFilterWhere(['like', 'comments', $this->comments])
            ->andFilterWhere(['like', 'active', $this->active]);

        return $dataProvider;
    }
}
