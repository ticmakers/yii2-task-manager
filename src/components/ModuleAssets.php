<?php

namespace ticmakers\taskmanager\components;

use yii\web\AssetBundle;

/**
 * AssetBundle ModuleAssets.
 *
 * @package ticmakers/taskmanager
 * @subpackage components
 * @category AssetBundle
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class ModuleAssets extends AssetBundle
{
    public $sourcePath = '@ticmakers/taskmanager/assets/';
    public $css = [
        'module.css'
    ];
    public $js = [
        'module.js',
    ];
    public $depends = [
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
        'ticmakers\core\web\BaseAsset'
    ];
}
