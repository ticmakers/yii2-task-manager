<?php

namespace ticmakers\taskmanager\components;

use ticmakers\core\base\Model as ModelBase;
use Yii;
use ticmakers\taskmanager\Module;

/**
 * Modelo base
 *
 * @package ticmakers/taskmanager
 * @subpackage components
 * @category Components
 *
 * @property string $titleReportExport Almacena el nombre del reporte generado con kartik-yii2-export
 * @property-read string STATUS_ACTIVE Estado activo según la columna.
 * @property-read string STATUS_INACTIVE Estado inactivo según la columna.
 * @property-read string STATUS_COLUMN Nombre de columna de estado.
 * @property-read string CREATED_AT_COLUMN Nombre de columna de Creado por.
 * @property-read string CREATED_DATE_COLUMN Nombre de columna de Fecha creación.
 * @property-read string UPDATED_AT_COLUMN Nombre de columna de Modificado por.
 * @property-read string UPDATED_DATE_COLUMN Nombre de columna de Fecha modificación.
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class Model extends ModelBase
{
    /**
     * Modulo padre de la instancia actual
     */

    public $module;

    /**
     * Configuración inicial.
     *
     * @return null
     */
    public function init()
    {
        parent::init();
        $this->module = (Module::getInstance()) ?? Yii::$app;
    }
}
