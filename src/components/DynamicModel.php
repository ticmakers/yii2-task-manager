<?php

namespace ticmakers\taskmanager\components;

use yii\base\DynamicModel as DynamicModelBase;

/**
 * Modelo base
 *
 * @package ticmakers
 * @subpackage taskmanager
 * @category Components
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author  Juan Sebastian Muñoz Reyes <juan.munoz@ticmakers.com>
 * @author  kevin Daniel Guzman Delgadillo <kevin.guzman@ticmakers.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class DynamicModel extends DynamicModelBase
{
    protected $_labels;

    public function setAttributeLabels($labels)
    {
        $this->_labels = $labels;
    }

    public function getAttributeLabel($name)
    {
        return $this->_labels[$name] ?? $name;
    }

}
