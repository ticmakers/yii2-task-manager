<?php

namespace ticmakers\taskmanager\components;

use Yii;
use yii\base\Component;
use yii\console\Exception;

/**
 * Clase que permite conocer y manipular algunos aspectos del sistema en el que
 * se este ejecutando la aplicación
 * Yii::$app->taskManager->appendCronjob(['0-59 * * * * >> tiempo.txt']);
 * Yii::$app->taskManager->removeCronjob(['0-59 * * * * >> tiempo.txt']);
 * Yii::$app->taskManager->modifyTask('0-51 * * * * >> tiempo.txt', '0-55 * * * * >> tiempo.txt');
 * Yii::$app->taskManager->exec('crontab -l');
 * Yii::$app->taskManager->getOutput();
 */
class TaskManager extends Component
{

    private $_connection; //Representa la conexión / recurso.
    private $_path; //Representará¡ la ruta al archivo.
    private $_handle; //Representará¡ el nombre de nuestro archivo cron temporal.
    private $_cronFile; //Representa la ruta completa y nombre del archivo cron temporal.
    private $_output; //Representa la salida del comando ejecutado
    public $host;
    public $port;
    public $username;
    public $password;
    public $config;

    /*
    $host, representa la direcciÃ³n IP del servidor remoto que desea conectarse
    $port, es el puerto que se utilizarÃ¡ para la conexiÃ³n SSH2
    $username, representarÃ¡ el nombre del usuario para acceder al servidor
    $password, representa la contraseÃ±a del usuario para acceder al servidor.
     */

    public function __construct($config = [])
    {

        parent::__construct($config);

        $this->_path = '';
        $this->_handle = 'crontab.txt';
        $this->_cronFile = "{$this->_path}{$this->_handle}";

        try {
            if (is_null($this->host) || is_null($this->port) || is_null($this->username) || is_null($this->password)) {
                throw new Exception("Please specify the host, port, username and password!");
            }

            $this->_connection = \ssh2_connect($this->host, $this->port);
            if (!$this->_connection) {
                throw new Exception("The SSH2 connection could not be established.");
            }

            $authentication = @ssh2_auth_password($this->_connection, $this->username, $this->password);

            if (!$authentication) {
                throw new Exception("Could not authenticate '{$this->username}' using password: '{$this->password}'.");
            }
        } catch (Exception $e) {
            $this->errorMessage($e->getMessage());
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function exec()
    {
        $argument_count = func_num_args();

        try {
            if (!$argument_count) {
                throw new Exception("There is nothing to execute, no arguments specified.");
            }

            $arguments = func_get_args();

            $command_string = ($argument_count > 1) ? implode(" && ", $arguments) : $arguments[0];

            $this->_output = @ssh2_exec($this->_connection, $command_string);

            if (!$this->_output) {
                throw new Exception("Unable to execute the specified commands: {$command_string}");
            }

            stream_set_blocking($this->_output, true);
            $stream_out = ssh2_fetch_stream($this->_output, SSH2_STREAM_STDIO);
            $this->_output = stream_get_contents($stream_out);
        } catch (Exception $e) {
            $this->errorMessage($e->getMessage());
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param [type] $path
     * @param [type] $handle
     * @return void
     */
    public function writeToFile($path = null, $handle = null)
    {
        if (!$this->crontabFileExists()) {
            $this->_handle = (is_null($handle)) ? $this->_handle : $handle;
            $this->_path = (is_null($path)) ? $this->_path : $path;

            $this->_cronFile = "{$this->_path}{$this->_handle}";

            $init_cron = "crontab -l > {$this->_cronFile} && [ -f {$this->_cronFile} ] || > {$this->_cronFile}";

            $this->exec($init_cron);
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function removeFile()
    {
        if ($this->crontabFileExists()) {
            $this->exec("rm -rf {$this->_cronFile}");
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param [type] $cronJobs
     * @return void
     */
    public function appendCronjob($cronJobs = null)
    {

        if (is_null($cronJobs)) {
            $this->errorMessage("Nothing to append!  Please specify a cron job or an array of cron jobs.");
        }

        $appendCronfile = "echo '";

        $appendCronfile .= (is_array($cronJobs)) ? implode(PHP_EOL, $cronJobs) : $cronJobs . PHP_EOL;

        $appendCronfile .= "'  >> {$this->_cronFile}";

        $this->exec($appendCronfile);

        $installCron = "crontab {$this->_cronFile}";

        $this->writeToFile();
        $this->exec($appendCronfile, $installCron);
        $this->removeFile();

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param [type] $cronJobs
     * @return void
     */
    public function removeCronjob($cronJobs = null)
    {
        if (is_null($cronJobs)) {
            $this->errorMessage("Nothing to remove!  Please specify a cron job or an array of cron jobs.");
        }

        $this->writeToFile();

        $fileLocal = Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . $this->_cronFile;
        ssh2_scp_recv($this->_connection, $this->_cronFile, $fileLocal);

        $cronArray = file(
            $fileLocal,
            FILE_IGNORE_NEW_LINES
        );

        if (empty($cronArray)) {
            $this->removeFile()->errorMessage("Nothing to remove!  The cronTab is already empty.");
        }

        if (is_array($cronJobs)) {
            foreach ($cronArray as $key => $value) {
                if (trim($value) == '') {
                    unset($cronArray[$key]);
                    continue;
                }
                foreach ($cronJobs as $key2 => $value2) {
                    if ($value == $value2) {
                        unset($cronArray[$key]);
                    }
                }
            }
        } else {

            foreach ($cronArray as $key => $value) {
                if (trim($value) == '') {
                    unset($cronArray[$key]);
                    continue;
                }
                if ($value == $cronJobs) {
                    unset($cronArray[$key]);
                }
            }
        }

        $this->removeFile()->exec("crontab -r");

        if (count($cronArray > 0)) {
            $this->appendCronjob($cronArray);
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function removeCrontab()
    {
        $this->removeFile()->exec("crontab -r");
        return $this;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    private function crontabFileExists()
    {
        $comando = "if [ -f {$this->_cronFile} ];
            then
                echo -e 'S';
            else
                echo -e 'N';
            fi";
        $this->exec($comando);

        return $this->_output == 'S';
    }

    /**
     * Undocumented function
     *
     * @param [type] $error
     * @return void
     */
    private function errorMessage($error)
    {
        throw new Exception($error);
    }

    /**
     * Permite obtener la salida de la ejecución de la tarea
     *
     * @return void
     */
    public function getOutput()
    {
        return $this->_output;
    }

    /**
     * Permite modificar una tarea programada
     *
     * @param string $oldTask
     * @param string $newTask
     * @return void
     */
    public function modifyTask($oldTask, $newTask)
    {
        return $this->removeCronjob($oldTask)->appendCronjob($newTask);
    }
}
