<?php
namespace ticmakers\taskmanager\commands;

use yii\console\Controller;
use Yii;

/**
 * Controlador de consola que agrupa acciones para sembrar datos en la aplicación.
 *
 * @package ticmakers/taskmanager
 * @subpackage commands
 * @category Commands
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @version 0.0.1
 * @since 1.0.0
 */

class TaskController extends Controller
{

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return void
     */
    public function actionRun($id)
    {

        $model = Yii::$app->db
            ->createCommand(
            "select script from scheduled_tasks task where task.active='Y' AND task.scheduled_task_id =:taskId",
                [':taskId' => $id]
            )
            ->queryOne();

        if ($model) {
            $class = trim($model['script']);
            if (class_exists($class)) {
                $model = Yii::createObject($class);
                $model->run($id);
            }
        }
    }
}
