<?php

namespace ticmakers\taskmanager\controllers;

use Yii;
use ticmakers\core\base\Controller;
use ticmakers\taskmanager\models\base\TaskPeriodicities;
use ticmakers\taskmanager\models\searchs\TaskPeriodicities as TaskPeriodicitiesSearch;

/**
 * Controlador TaskPeriodicitiesController implementa las acciones para el CRUD de el modelo TaskPeriodicities.
 *
 * @package app
 * @subpackage controllers
 * @category Controllers
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TIC Makers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class TaskPeriodicitiesController extends Controller
{
    public $isModal = true;
    public $model = TaskPeriodicities::class;
    public $searchModel = TaskPeriodicitiesSearch::class;
}
