<?php
namespace ticmakers\taskmanager\controllers\actions;

class RunTaskAction extends yii\base\Action
{

    /**
     * Permite ejecutar la acción para ejecutar una tarea programada
     *
     * @return mixed
     */
    public function run()
    { }
}
