<?php

namespace ticmakers\taskmanager\controllers;

use Yii;
use ticmakers\core\base\Controller;
use ticmakers\taskmanager\models\base\ScheduledTaskLogs;
use ticmakers\taskmanager\models\searchs\ScheduledTaskLogs as ScheduledTaskLogsSearch;

/**
 * Controlador ScheduledTaskLogsController implementa las acciones para el CRUD de el modelo ScheduledTaskLogs.
 *
 * @package app
 * @subpackage controllers
 * @category Controllers
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TIC Makers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class ScheduledTaskLogsController extends Controller
{
    public $isModal = true;
    public $model = ScheduledTaskLogs::class;
    public $searchModel = ScheduledTaskLogsSearch::class;

    /**
     * Lista todos registros según el DataProvider.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = Yii::createObject($this->searchModel);
        $paramsGet = Yii::$app->request->get();
        if (!empty($paramsGet)) {
            $searchModel->setAttributes($paramsGet);
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax(
            'index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }
}
