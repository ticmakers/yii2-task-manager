<?php

namespace ticmakers\taskmanager\controllers;

use Yii;
use ticmakers\core\base\Controller;
use ticmakers\taskmanager\models\app\TaskPeriodicities;
use yii\web\Response;

/**
 * Controlador ScheduledTaskController implementa las acciones para el CRUD de el modelo ScheduledTask.
 *
 * @package app
 * @subpackage controllers
 * @category Controllers
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S. 
 * @version 0.0.1
 * @since 1.0.0
 */
class ScheduledTaskController extends Controller
{
    public $modelClass = \ticmakers\taskmanager\models\app\ScheduledTasks::class;
    public $searchModelClass = \ticmakers\taskmanager\models\searchs\ScheduledTask::class;

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return void
     */
    public function actionConfig($id)
    {
        if (!empty($id)) {
            $model = TaskPeriodicities::findOne($id);
            $res = [];
            if ($model) {
                $valores = explode(' ', $model->value);

                $keys = [
                    TaskPeriodicities::TYPE_MINUTE,
                    TaskPeriodicities::TYPE_HOUR,
                    TaskPeriodicities::TYPE_DAY,
                    TaskPeriodicities::TYPE_MONTH,
                    TaskPeriodicities::TYPE_WEEKDAY,
                ];
                for ($i = 0; $i <= 4; $i++) {
                    $periodicidad = TaskPeriodicities::find()->where([
                        'active' => TaskPeriodicities::STATUS_ACTIVE,
                        'type' => $keys[$i],
                        'value' => $valores[$i]
                    ])->one();

                    $res[$keys[$i]] = $periodicidad->primaryKey;
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            return $res;
        }
    }

    /**
     * Permite la ejecución de una tarea programada
     * @param integer $id
     * @return mixed
     */
    public function actionRun($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);
        // try {

        $class = trim($model->script);
        if (class_exists($class)) {
            $model = Yii::createObject($class);
            $resultado = $model->run($id);
            if ($resultado) {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t('app', 'La tarea programada se ejecutó de manera correcta'));
            } else {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t('app', 'Se ha presentado un error en la ejecución de la tarea programada, por favor revise el log para mayor información'));
            }
        } else {
            Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t('app', 'Por favor verifique la configuración de la tarea (WEB)'));
        }
        // } catch (yii\base\Exception $ex) {
        //     Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t('app', 'Por favor verifique la configuración de la tarea (WEB)'));
        // }

        return $this->redirect(['index']);
    }
}
