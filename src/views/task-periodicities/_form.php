<?php

use ticmakers\core\widgets\ActiveForm;
use ticmakers\core\helpers\UI;

/* @var $this yii\web\View */
/* @var $model ticmakers\taskmanager\models\base\TaskPeriodicities */
/* @var $form ticmakers\core\widgets\ActiveForm */
?>

<div class="task-periodicities-form">
    <?php $form = ActiveForm::begin([
        'id' => 'task-periodicities-form',
    ]); ?>
    <div class="row">
            
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'type',['help' => '','popover' => $model->getHelp('type')])->textInput(['maxlength' => true, 'tabindex'=>1]) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'value',['help' => '','popover' => $model->getHelp('value')])->textInput(['maxlength' => true, 'tabindex'=>2]) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'description',['help' => '','popover' => $model->getHelp('description')])->textInput(['maxlength' => true, 'tabindex'=>3]) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'selectable',['help' => '','popover' => $model->getHelp('selectable')])->textInput(['maxlength' => true, 'tabindex'=>4]) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'created_at',['help' => '','popover' => $model->getHelp('created_at')])->textInput(['tabindex'=>5]) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'updated_by',['help' => '','popover' => $model->getHelp('updated_by')])->textInput(['tabindex'=>6]) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'updated_at',['help' => '','popover' => $model->getHelp('updated_at')])->textInput(['tabindex'=>7]) ?>

			</div>

    </div>

    <?php ActiveForm::end(); ?>
</div>
