<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model ticmakers\taskmanager\models\base\TaskPeriodicities */

 ?>
<div class="task-periodicities-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'task_periodicity_id',
            'type',
            'value',
            'description',
            'selectable',
            [
                'attribute' => 'active',
                'value'     => Yii::$app->strings::getCondition($model->active),
            ],
            [
                'attribute' => 'created_by',
                'value'     => $model->creadoPor->username,
            ],
            'created_at',
            [
                'attribute' => 'updated_by',
                'value'     => $model->modificadoPor->username,
            ],
            'updated_at',
        ],
    ]) ?>

</div>
