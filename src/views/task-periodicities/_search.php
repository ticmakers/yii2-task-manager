<?php

use ticmakers\core\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model ticmakers\taskmanager\models\searchs\TaskPeriodicities */
/* @var $form ticmakers\core\widgets\ActiveForm */
?>

<div class="task-periodicities-search">
    <?php $form = ActiveForm::begin([
    'id' => 'task-periodicities-search-form',
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    <div class="row">
        
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'type',['help' => '','popover' => $model->getHelp('type')])->textInput(['tabindex' => 1, 'id' => 'type_search']) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'value',['help' => '','popover' => $model->getHelp('value')])->textInput(['tabindex' => 2, 'id' => 'value_search']) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'description',['help' => '','popover' => $model->getHelp('description')])->textInput(['tabindex' => 3, 'id' => 'description_search']) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'selectable',['help' => '','popover' => $model->getHelp('selectable')])->textInput(['tabindex' => 4, 'id' => 'selectable_search']) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'active',['help' => '','popover' => $model->getHelp('active')])->widget(\kartik\widgets\Select2::classname(),
                        [
                            'data' => Yii::$app->strings::getCondition(),
                            'options' => ['placeholder' => Yii::$app->strings::getTextAll(), 'tabindex' => 5, 'id' => 'active_search'],
                        ]
                    ) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'created_by',['help' => '','popover' => $model->getHelp('created_by')])->widget(\kartik\widgets\Select2::classname(),
                        ['data' => Yii::$app->userHelper::getUsers(), 'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 6, 'id' => 'created_by_search']]
                    ) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'created_at',['help' => '','popover' => $model->getHelp('created_at')])->widget(\kartik\widgets\DatePicker::classname(), ['options' => ['tabindex' => 7, 'id' => 'created_at_search']]) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'updated_by',['help' => '','popover' => $model->getHelp('updated_by')])->widget(\kartik\widgets\Select2::classname(),
                        ['data' => Yii::$app->userHelper::getUsers(), 'options' => ['placeholder' => Yii::$app->strings::getTextEmpty(), 'tabindex' => 8, 'id' => 'updated_by_search']]
                    ) ?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?= $form->field($model, 'updated_at',['help' => '','popover' => $model->getHelp('updated_at')])->widget(\kartik\widgets\DatePicker::classname(), ['options' => ['tabindex' => 9, 'id' => 'updated_at_search']]) ?>

			</div>

</div>


<?php ActiveForm::end(); ?>
</div> 