<?php

use kartik\dynagrid\DynaGrid;
use yii\bootstrap4\Modal;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel ticmakers\taskmanager\models\searchs\TaskPeriodicities */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Task Periodicities');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-periodicities-index">
	<?php echo DynaGrid::widget([
		'columns' => [
			['class' => 'kartik\grid\SerialColumn'],
			[
				'attribute' => 'task_periodicity_id',
				'visible' => false,
			],
			[
				'attribute' => 'type',
				'visible' => true,
			],
			[
				'attribute' => 'value',
				'visible' => true,
			],
			[
				'attribute' => 'description',
				'visible' => true,
			],
			[
				'attribute' => 'selectable',
				'visible' => true,
			],
			[
				'attribute' => 'active',
				'visible' => false,
				'value' => function ($model) {
					return Yii::$app->strings::getCondition($model->active);
				},
				'filterType' => GridView::FILTER_SELECT2,
				'filter' => Yii::$app->strings::getCondition(),
				'filterInputOptions' => ['id' => 'active_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
				'hiddenFromExport' => true,
			],
			[
				'attribute' => 'created_by',
				'visible' => false,
				'value' => function ($model) {
					return $model->creadoPor->username;
				},
				'filterType' => GridView::FILTER_SELECT2,
				'filter' => Yii::$app->userHelper::getUsers(),
				'filterInputOptions' => ['id' => 'created_by_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
				'hiddenFromExport' => true,
			],
			[
				'attribute' => 'created_at',
				'visible' => false,
				'filterType' => GridView::FILTER_DATE,
				'filterInputOptions' => ['id' => 'created_at_grid'],
				'hiddenFromExport' => true,
			],
			[
				'attribute' => 'updated_by',
				'visible' => false,
				'value' => function ($model) {
					return $model->modificadoPor->username;
				},
				'filterType' => GridView::FILTER_SELECT2,
				'filter' => Yii::$app->userHelper::getUsers(),
				'filterInputOptions' => ['id' => 'updated_by_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
				'hiddenFromExport' => true,
			],
			[
				'attribute' => 'updated_at',
				'visible' => false,
				'filterType' => GridView::FILTER_DATE,
				'filterInputOptions' => ['id' => 'updated_at_grid'],
				'hiddenFromExport' => true,
			],
			[
				'class' => 'ticmakers\core\base\ActionColumn',
				'isModal' => true,
				'dynaModalId' => 'task-periodicities',

			],
		],
		'gridOptions' => [
			'panelHeadingTemplate' => "<h3>{$this->title}</h3>",
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'toolbar' => [
				'{toggleData}',
				[
					'content' =>
					Yii::$app->ui::btnNew('', ['create'], ['onClick' => "openModalGrid(this, 'task-periodicities', 'create'); return false;"]) . ' '
						. Yii::$app->ui::btnSearch('') . ' '
						. Yii::$app->ui::btnRefresh('')
				],
			],
			'pjax' => true,
			'options' => ['id' => 'gridview-task-periodicities'],
		],
		'options' => ['id' => 'dynagrid-task-periodicities'],
	]) ?>
</div>
<?php Modal::begin([
	'id'      => 'search-modal',
	'title'  => Yii::t('app', 'Búsqueda Avanzada'),
	'size'    => Modal::SIZE_LARGE,
	'footer' => Yii::$app->ui::btnCloseModal(Yii::t('app', 'Cancelar'), Yii::$app->html::ICON_REMOVE) . Yii::$app->ui::btnSend(Yii::t('app', 'Buscar'),  ['form' => 'task-periodicities-search-form']),
	'options' => ['style' => 'display: none;', 'tabindex' => '-1']
]);
echo $this->render('_search', ['model' => $searchModel]);
Modal::end();
?>
<?php Modal::begin([
	'id'            => 'task-periodicities-modal',
	'title'        => 'Task Periodicity',
	'size'    => Modal::SIZE_LARGE,
	'footer' => Yii::$app->ui::btnCloseModalMessage(Yii::t('app', 'Cancelar'), Yii::$app->html::ICON_REMOVE) . Yii::$app->ui::btnSend(null,  ['form' => 'task-periodicities-form']),
	'closeButton' => ['class' => 'close confirm-close'],
	'options'       => ['style' => 'display: none;', 'tabindex' => '-1'],
]);

Modal::end();
?>