<?php
use ticmakers\core\helpers\Html;
use yii\helpers\ArrayHelper;

$breadcrumbs = [];
$this->title = Yii::t(Yii::$app->controller->module->id, 'Task manager');
$this->params['breadcrumbs'] = [];
$this->params['breadcrumbs'] = ArrayHelper::merge($this->params['breadcrumbs'], $breadcrumbs);
$this->params['breadcrumbs'][] = $this->title;
