<?php

use ticmakers\core\widgets\ActiveForm;
use ticmakers\taskmanager\models\app\TaskPeriodicities;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model ticmakers\taskmanager\models\base\ScheduledTask */
/* @var $form ticmakers\core\widgets\ActiveForm */
?>

<div class="scheduled-task-form">
	<?php $form = ActiveForm::begin([
		'id' => 'scheduled-task-form',
	]); ?>
	<div class="row">

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?= $form->field($model, 'name', ['help' => '', 'popover' => $model->getHelp('name')])->textInput([
				'maxlength' => true,
				'tabindex' => 1,
				'readonly' => !$model->isNewRecord
			]) ?>

		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<?php /*$form->field($model, 'script', ['help' => '', 'popover' => $model->getHelp('script')])->textInput([
						'maxlength' => true,
						'tabindex' => 2,
						'readonly' => !$model->isNewRecord
					])*/ ?>
			<?= $form->field($model, 'script', ['help' => '', 'popover' => $model->getHelp('script')])->widget(\kartik\select2\Select2::class, [
				'disabled' => !$model->isNewRecord,
				'data' => Yii::$app->getModule('taskmanager')->getModelsTask()
			]) ?>

		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<?= $form->field($model, 'web', ['help' => '', 'popover' => $model->getHelp('web')])->widget(\kartik\select2\Select2::class, [

				'disabled' => !$model->isNewRecord,

				'data' => Yii::$app->getModule('taskmanager')->getModelsTask()
			]) ?>

		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?= $form->field($model, 'description', ['help' => '', 'popover' => $model->getHelp('description')])->textarea([
				'rows' => 3,
				'tabindex' => 4,
				'readonly' => !$model->isNewRecord
			]) ?>

		</div>

		<div class="col-12">
			<h4 style="border-bottom: 1px dashed #eeeeee;padding-bottom: 10px;margin-bottom: 15px;"><?= Yii::t('taskmanager', 'Periodicity') ?></h4>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<?= $form->field($model, 'minute', ['help' => '', 'popover' => $model->getHelp('minute')])->widget(
				Select2::class,
				[
					'language' => 'es',
					'data' => TaskPeriodicities::getData(
						true,
						[
							'type' => TaskPeriodicities::TYPE_MINUTE,
							'selectable' => TaskPeriodicities::STATUS_ACTIVE,
						]
					),
					'options' => [
						'placeholder' => TaskPeriodicities::getData(
							false,
							[
								'type' => TaskPeriodicities::TYPE_MINUTE,
								'selectable' => TaskPeriodicities::STATUS_INACTIVE,
							]
						)[0]->description ?? ''
					],
					'pluginOptions' => [
						'allowClear' => true,
					],
				]
			); ?>

		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<?= $form->field($model, 'hour', ['help' => '', 'popover' => $model->getHelp('hour')])->widget(
				Select2::class,
				[
					'language' => 'es',
					'data' => TaskPeriodicities::getData(
						true,
						[
							'type' => TaskPeriodicities::TYPE_HOUR,
							'selectable' => TaskPeriodicities::STATUS_ACTIVE,
						]
					),
					'options' => [
						'placeholder' => TaskPeriodicities::getData(
							false,
							[
								'type' => TaskPeriodicities::TYPE_HOUR,
								'selectable' => TaskPeriodicities::STATUS_INACTIVE,
							]
						)[0]->description ?? ''
					],
					'pluginOptions' => [
						'allowClear' => true,
					],
				]
			);  ?>

		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<?= $form->field($model, 'day', ['help' => '', 'popover' => $model->getHelp('day')])->widget(
				Select2::class,
				[
					'language' => 'es',
					'data' => TaskPeriodicities::getData(
						true,
						[
							'type' => TaskPeriodicities::TYPE_DAY,
							'selectable' => TaskPeriodicities::STATUS_ACTIVE,
						]
					),
					'options' => [
						'placeholder' => TaskPeriodicities::getData(
							false,
							[
								'type' => TaskPeriodicities::TYPE_DAY,
								'selectable' => TaskPeriodicities::STATUS_INACTIVE,
							]
						)[0]->description ?? ''
					],
					'pluginOptions' => [
						'allowClear' => true,
					],
				]
			);  ?>

		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<?= $form->field($model, 'month', ['help' => '', 'popover' => $model->getHelp('month')])->widget(
				Select2::class,
				[
					'language' => 'es',
					'data' => TaskPeriodicities::getData(
						true,
						[
							'type' => TaskPeriodicities::TYPE_MONTH,
							'selectable' => TaskPeriodicities::STATUS_ACTIVE,
						]
					),
					'options' => [
						'placeholder' => TaskPeriodicities::getData(
							false,
							[
								'type' => TaskPeriodicities::TYPE_MONTH,
								'selectable' => TaskPeriodicities::STATUS_INACTIVE,
							]
						)[0]->description ?? ''
					],
					'pluginOptions' => [
						'allowClear' => true,
					],
				]
			);  ?>

		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<?= $form->field($model, 'weekday', ['help' => '', 'popover' => $model->getHelp('weekday')])->widget(
				Select2::class,
				[
					'language' => 'es',
					'data' => TaskPeriodicities::getData(
						true,
						[
							'type' => TaskPeriodicities::TYPE_WEEKDAY,
							'selectable' => TaskPeriodicities::STATUS_ACTIVE,
						]
					),
					'options' => [
						'placeholder' => TaskPeriodicities::getData(
							false,
							[
								'type' => TaskPeriodicities::TYPE_WEEKDAY,
								'selectable' => TaskPeriodicities::STATUS_INACTIVE,
							]
						)[0]->description ?? ''
					],
					'pluginOptions' => [
						'allowClear' => true,
					],
				]
			);  ?>

		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<?=
				$form->field($model, 'commonConfig')->widget(
					Select2::class,
					[
						'language' => 'es',
						'data' => TaskPeriodicities::getData(
							true,
							[
								'type' => TaskPeriodicities::TYPE_COMMON,
								'selectable' => TaskPeriodicities::STATUS_ACTIVE,
							]
						),
						'options' => [
							'placeholder' => TaskPeriodicities::getData(
								false,
								[
									'type' => TaskPeriodicities::TYPE_COMMON,
									'selectable' => TaskPeriodicities::STATUS_INACTIVE,
								]
							)[0]->description ?? '',
							'onchange' => '
                $.post( "' . Yii::$app->urlManager->createUrl('/taskmanager/scheduled-task/config?id=') . '"+$(this).val(), function( data ) {

                    $( "#' . Yii::$app->html::getInputId($model, 'minute') . '" ).val(data.' . TaskPeriodicities::TYPE_MINUTE . ');
                    $( "#' . Yii::$app->html::getInputId($model, 'minute') . '" ).trigger("change");
                    $( "#' . Yii::$app->html::getInputId($model, 'hour') . '" ).val(data.' . TaskPeriodicities::TYPE_HOUR . ');
                    $( "#' . Yii::$app->html::getInputId($model, 'hour') . '" ).trigger("change");
                    $( "#' . Yii::$app->html::getInputId($model, 'day') . '" ).val(data.' . TaskPeriodicities::TYPE_DAY . ');
                    $( "#' . Yii::$app->html::getInputId($model, 'day') . '" ).trigger("change");
                    $( "#' . Yii::$app->html::getInputId($model, 'month') . '" ).val(data.' . TaskPeriodicities::TYPE_MONTH . ');
                    $( "#' . Yii::$app->html::getInputId($model, 'month') . '" ).trigger("change");
                    $( "#' . Yii::$app->html::getInputId(
								$model,
								'weekday'
							) . '" ).val(data.' . TaskPeriodicities::TYPE_WEEKDAY . ');
                    $( "#' . Yii::$app->html::getInputId(
								$model,
								'weekday'
							) . '" ).trigger("change");

                });',
						],
						'pluginOptions' => [
							'allowClear' => true,
						],
					]
				);
			?>
		</div>
	</div>

	<div class="form-group text-right">
		<?= Yii::$app->ui::btnCancel(); ?>
		<?= Yii::$app->ui::btnSend(); ?>
	</div>

	<?php ActiveForm::end(); ?>
</div>