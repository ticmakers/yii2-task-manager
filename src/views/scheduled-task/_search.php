<?php

use ticmakers\core\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model ticmakers\taskmanager\models\searchs\ScheduledTask */
/* @var $form ticmakers\core\widgets\ActiveForm */
?>

<div class="scheduled-task-search">
	<?php $form = ActiveForm::begin([
		'id' => 'scheduled-task-search-form',
		'action' => ['index'],
		'method' => 'get',
	]); ?>

	<div class="row">

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<?= $form->field($model, 'name', ['help' => '', 'popover' => $model->getHelp('name')])->textInput(['tabindex' => 1, 'id' => 'name_search']) ?>

		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<?= $form->field($model, 'active', ['help' => '', 'popover' => $model->getHelp('active')])->widget(
				\kartik\widgets\Select2::classname(),
				[
					'data' => Yii::$app->strings::getCondition(),
					'options' => ['placeholder' => Yii::$app->strings::getTextAll(), 'tabindex' => 10, 'id' => 'active_search'],
				]
			) ?>

		</div>
	</div>


	<?php ActiveForm::end(); ?>
</div>