<?php

use kartik\dynagrid\DynaGrid;
use yii\bootstrap4\Modal;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel ticmakers\taskmanager\models\searchs\ScheduledTaskLogs */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Log {task}', ['task' => $searchModel->scheduledTask->name ?? '']);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scheduled-task-logs-index">
	<?php echo DynaGrid::widget([
		'columns' => [
			['class' => 'kartik\grid\SerialColumn'],
			[
				'attribute' => 'scheduled_task_log_id',
				'visible' => false,
			],
			[
				'attribute' => 'scheduled_task_id',
				'visible' => false,
				'value' => function ($model) {
					if (!empty($model->scheduledTask)) {
						$name = $model->scheduledTask->getNameFromRelations();
						return $model->scheduledTask->$name;
					}
				},
				'filterType' => GridView::FILTER_SELECT2,
				'filter' => ticmakers\taskmanager\models\app\ScheduledTasks::getData(),
				'filterInputOptions' => ['id' => 'scheduled_task_id_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
			],
			[
				'attribute' => 'created_at',
				'visible' => true,
				'filterType' => GridView::FILTER_DATE,
				'filterInputOptions' => ['id' => 'created_at_grid'],
				'hiddenFromExport' => true,
			],
			[
				'attribute' => 'result',
				'value' => function ($model) {

					return ticmakers\taskmanager\models\task\TaskBase::getResultStatus($model->result, true);
				},
				'visible' => true,
				'filterType' => GridView::FILTER_SELECT2,
				'filter' => ticmakers\taskmanager\models\task\TaskBase::getResultStatus(),
				'filterInputOptions' => ['id' => 'scheduled_task_id_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
			],
			[
				'attribute' => 'comments',
				'format' => 'ntext',
				'visible' => true,
			],
			[
				'attribute' => 'active',
				'visible' => false,
				'value' => function ($model) {
					return Yii::$app->strings::getCondition($model->active);
				},
				'filterType' => GridView::FILTER_SELECT2,
				'filter' => Yii::$app->strings::getCondition(),
				'filterInputOptions' => ['id' => 'active_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
				'hiddenFromExport' => true,
			],
			[
				'attribute' => 'created_by',
				'visible' => false,
				'value' => function ($model) {
					return $model->creadoPor->username;
				},
				'filterType' => GridView::FILTER_SELECT2,
				'filter' => Yii::$app->userHelper::getUsers(),
				'filterInputOptions' => ['id' => 'created_by_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
				'hiddenFromExport' => true,
			],
			[
				'attribute' => 'updated_by',
				'visible' => false,
				'value' => function ($model) {
					return $model->modificadoPor->username;
				},
				'filterType' => GridView::FILTER_SELECT2,
				'filter' => Yii::$app->userHelper::getUsers(),
				'filterInputOptions' => ['id' => 'updated_by_grid', 'placeholder' => Yii::$app->strings::getTextAll()],
				'hiddenFromExport' => true,
			],
			[
				'attribute' => 'updated_at',
				'visible' => false,
				'filterType' => GridView::FILTER_DATE,
				'filterInputOptions' => ['id' => 'updated_at_grid'],
				'hiddenFromExport' => true,
			],
		],
		'theme' => 'simple-default',
		'gridOptions' => [
			'panelHeadingTemplate' => "<h3>{$this->title}</h3>",
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			// 'toolbar' => [
			//     '{toggleData}',
			//     [
			//         'content' =>
			//         	Yii::$app->ui::btnNew('', ['create'], ['onClick' => "openModalGrid(this, 'scheduled-task-logs', 'create'); return false;"]) . ' '
			//             .Yii::$app->ui::btnSearch('') . ' '
			//             .Yii::$app->ui::btnRefresh('')
			//     ],
			//     '{dynagrid}',
			//     '{export}',
			// ],
			'pjax' => true,
			'options' => ['id' => 'gridview-scheduled-task-logs'],
		],
		'options' => ['id' => 'dynagrid-scheduled-task-logs'],
	]) ?>
</div>