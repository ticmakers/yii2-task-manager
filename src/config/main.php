<?php

$params = require __DIR__ . '/params.php';

return [
    'components' => [
        // lista de configuraciones de componente
        'view' => [
            'class' => 'ticmakers\core\base\View',
            'theme' => [
                'pathMap' => [
                    '@ticmakers/taskmanager/views' => '@app/views',
                ],
            ],
        ],
    ],
    'params' => $params,
];
