<?php

namespace ticmakers\taskmanager\migrations;

use yii\db\Migration;

class m190522_220048_create_table_scheduled_tasks extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%scheduled_tasks}}', [
            'scheduled_task_id' => $this->primaryKey()->comment('Scheduled task identifier'),
            'name' => $this->string()->notNull()->comment('Scheduled task name'),
            'description' => $this->text()->comment('Description of the scheduled task'),
            'script' => $this->string()->notNull()->comment('Name of the script which calls the scheduled task.'),
            'web' => $this->string()->notNull()->comment('Option that should be called when the task is done manually from the web part.'),
            'minute' => $this->string()->notNull()->comment('Minute when the scheduled task is executed'),
            'hour' => $this->string()->notNull()->comment('Hour when the scheduled task is executed'),
            'day' => $this->string()->notNull()->comment('Day when the scheduled task is executed'),
            'month' => $this->string()->notNull()->comment('Month when the scheduled task is executed'),
            'weekday' => $this->string()->notNull()->comment('Weekday when the scheduled task is executed'),
            'active' => $this->char()->notNull()->comment('Define if it\'s active or not. Format : Y -> Yes, N -> No'),
            'created_by' => $this->integer()->notNull()->comment('It\'s the identifier of the user who created the record.'),
            'created_at' => $this->timestamp()->notNull()->comment('Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS'),
            'updated_by' => $this->integer()->notNull()->comment('It\'s the identifier of the user who updated the record.'),
            'updated_at' => $this->timestamp()->notNull()->comment('Define the update date and time. Format: YYYY-MM-DD HH: MM: SS'),
        ], $tableOptions);

        $this->execute("ALTER TABLE {{%scheduled_tasks}} ADD CONSTRAINT \"chk-scheduled_tasks.active\"
                        CHECK (active IN ('Y', 'N'))");

    }

    public function down()
    {
        $this->dropTable('{{%scheduled_tasks}}');
    }
}
