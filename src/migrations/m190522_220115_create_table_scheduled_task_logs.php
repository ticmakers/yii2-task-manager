<?php

namespace ticmakers\taskmanager\migrations;

use yii\db\Migration;

class m190522_220115_create_table_scheduled_task_logs extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%scheduled_task_logs}}', [
            'scheduled_task_log_id' => $this->primaryKey()->comment('Scheduled task log identifier'),
            'scheduled_task_id' => $this->integer()->notNull()->comment('scheduled task identifier'),
            'result' => $this->string()->notNull()->comment('It takes a two values:

1. Successful
2. Failed'),
            'comments' => $this->text()->comment('scheduled task log comments'),
            'active' => $this->char()->notNull()->comment('Define if it\'s active or not. Format : Y -> Yes, N -> No'),
            'created_by' => $this->integer()->notNull()->comment('It\'s the identifier of the user who created the record.'),
            'created_at' => $this->timestamp()->notNull()->comment('Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS'),
            'updated_by' => $this->integer()->notNull()->comment('It\'s the identifier of the user who updated the record.'),
            'updated_at' => $this->timestamp()->notNull()->comment('Define the update date and time. Format: YYYY-MM-DD HH: MM: SS'),
        ], $tableOptions);

        $this->execute("ALTER TABLE {{%scheduled_task_logs}} ADD CONSTRAINT \"chk-scheduled_task_logs.active\"
                        CHECK (active IN ('Y', 'N'))");
    }

    public function down()
    {
        $this->dropTable('{{%scheduled_task_logs}}');
    }
}
