<?php

namespace ticmakers\taskmanager\migrations;

use yii\db\Migration;

class m190522_220010_create_table_task_periodicities extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%task_periodicities}}', [
            'task_periodicity_id' => $this->primaryKey(),
            'type' => $this->string()->notNull(),
            'value' => $this->string()->notNull(),
            'description' => $this->string(),
            'selectable' => $this->char()->notNull(),
            'active' => $this->char()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ], $tableOptions);

        $this->execute('ALTER TABLE {{%task_periodicities}} ADD CONSTRAINT "chk-task_periodicities.active"
                        CHECK (active IN (\'Y\', \'N\'))');

    }

    public function safeDown()
    {
        $this->dropTable('{{%task_periodicities}}');
    }
}
