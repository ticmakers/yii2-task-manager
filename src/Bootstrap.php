<?php

namespace ticmakers\taskmanager;

use yii\base\Application;
use yii\base\BootstrapInterface;
use ticmakers\taskmanager\models\task\TaskBase;
use ticmakers\taskmanager\models\base\ScheduledTask as ScheduledTask;
use ticmakers\taskmanager\models\base\ScheduledTaskLogs as ScheduledTaskLogs;
use ticmakers\taskmanager\models\base\TaskPeriodicities as TaskPeriodicities;
use ticmakers\taskmanager\models\searchs\ScheduledTask as ScheduledTaskSearch;
use ticmakers\taskmanager\models\searchs\ScheduledTaskLogs as ScheduledTaskLogsSearch;
use ticmakers\taskmanager\models\searchs\TaskPeriodicities as TaskPeriodicitiesSearch;
use Yii;

/**
 * Class Bootstrap
 * @package ticmakers\yii2-taskmanager
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 */
class Bootstrap implements BootstrapInterface
{

    /**
     * @var array
     */
    private $_modelMap = [
        'base' => [
            'ScheduledTask' => ScheduledTask::class,
            'ScheduledTaskLogs' => ScheduledTaskLogs::class,
            'TaskPeriodicities' => TaskPeriodicities::class,
        ],
        'searchs' => [
            'ScheduledTask' => ScheduledTaskSearch::class,
            'ScheduledTaskLogs' => ScheduledTaskLogsSearch::class,
            'TaskPeriodicities' => TaskPeriodicitiesSearch::class,
        ],
        'task' => [
            'TaskBase' => TaskBase::class,
        ],
    ];

    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if (!$app->hasModule('taskmanager')) {
            $app->setModule('taskmanager', 'ticmakers\taskmanager\Module');
        }

        if ($app instanceof \yii\console\Application) {
            $app->getModule('taskmanager')->controllerNamespace = 'ticmakers\taskmanager\commands';
        }
            
        $this->overrideViews($app, 'taskmanager');
        $this->overrideModels($app, 'taskmanager');
    }

    /**
     * Undocumented function
     *
     * @param [type] $app
     * @param [type] $moduleId
     * @return void
     */
    private function overrideViews($app, $moduleId)
    {
        if ($app instanceof \yii\web\Application) {
            $app->getView()->theme->pathMap["@ticmakers/{$moduleId}/views"] = "@app/views/{$moduleId}";
        }
    }

    private function overrideModels($app, $moduleId)
    {
        /**
         * @var Module $module
         * @var ActiveRecord $modelName
         */
        if ($app->hasModule($moduleId) && ($module = $app->getModule($moduleId)) instanceof Module) {
            $this->_modelMap = array_merge($this->_modelMap, $module->modelMap);
            foreach ($this->_modelMap as $pathModel => $modelMap) {
                foreach ($modelMap as $name => $definition) {
                    $class = "ticmakers\\{$moduleId}\\models\\{$pathModel}\\" . $name;
                    Yii::$container->set($class, $definition);
                    $modelName = is_array($definition) ? $definition['class'] : $definition;
                    $module->modelMap[$name] = $modelName;
                }
            }
        }
    }
}
