<?php

namespace ticmakers\taskmanager;

use Yii;

/**
 * Taskmanager module definition class
 */
class Module extends \ticmakers\core\base\Module
{

    /**
     * Permite establecer el directorio en el cual se encontraran lo modelos de tipo tarea
     *
     * @var [type]
     */
    public $basePathTaskModels = '@app/models/task';
    
    /**
     * {@inheritdoc}
     *
     * @var string
     */
    public $defaultRoute = 'scheduled-task';

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'ticmakers\taskmanager\controllers';

    /**
     * Busca todos los archivos de modelos según el modelsPath
     *
     * @return array
     */
    public function getModelsTask()
    {
        $modelsList = [];
        $path = Yii::getAlias($this->basePathTaskModels);
        if (file_exists($path) && is_dir($path)) {
            foreach (scandir($path) as $file) {
                $filePath = "$path/$file";
                if (is_file($filePath) && pathinfo($file, PATHINFO_EXTENSION) === 'php') {
                    $class = str_replace(['@', '/'], ['', '\\'], $this->basePathTaskModels) . '\\' . str_replace('.php', '', $file);
                    if (is_subclass_of($class, \ticmakers\taskmanager\models\task\TaskBase::class)) {
                        $modelsList[$class] = str_replace('.php', '', $file);
                    }
                }
            }
        }
        return $modelsList;
    }
}
