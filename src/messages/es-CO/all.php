<?php
$app = require __DIR__ . '/taskmanager.php';
$core = require __DIR__ . '/../../../../yii2-core/src/messages/es-CO/app.php';

return Yii::$app->arrayHelper::merge($app, $core);
