<?php

return [
    // Custom messages 
    'Minute' => 'Minuto',
    'Hour' => 'Hora',
    'Day' => 'Día',
    'Month' => 'Mes',
    'Weekday' => 'Día de la semana',
    'Common config' => 'Configuración común',
    'Result' => 'Resultado',
    'Successful' => 'Exitoso',
    'Failed' => 'Fallido',
    'Periodicity' => 'Periodicidad',
    'Last run' => 'Última ejecución',
    'Date and time' => 'Fecha y hora',
    '"{function}" is not implemented.' => '"{function}" is not implemented.',
    'Comments' => 'Observaciones',
    'Edit' => 'Editar',
    'Scheduled tasks' => 'Tareas programadas',
    'Scheduled task' => 'Tarea programada',
    'Create {option}' => 'Crear {option}',

    'Scheduled task identifier' => 'Identificador de la tarea programada',
    'Information provided by the social network' => 'Información proporcionada por la red social.',
    'Name of the script which calls the scheduled task.' => 'Nombre del script que llama a la tarea programada.',
    'Option that should be called when the task is done manually from the web part.' => 'Opción que debe llamarse cuando la tarea se realiza manualmente desde el elemento web.',
    'Minute when the scheduled task is executed' => 'Minuto cuando se ejecuta la tarea programada',
    'Hour when the scheduled task is executed' => 'Hora en que se ejecuta la tarea programada.',
    'Day when the scheduled task is executed' => 'Día en que se ejecuta la tarea programada.',
    'Month when the scheduled task is executed' => 'Mes en que se ejecuta la tarea programada',
    'Weekday when the scheduled task is executed' => 'Día de la semana cuando se ejecuta la tarea programada.',

    'It takes a two values:  1. Successful 2. Failed' => 'Toma dos valores: 1. Exitoso 2. Fallido',
    'Scheduled task log comments' => 'Comentarios de registro de tareas programadas',

    'Type of periodicity that is applied to the value for the scheduled task.  MIN->Minute HOU->Hour DAY->Day MON->Month WEEK->Weekdays COM-> Common' => 'Tipo de periodicidad que se aplica al valor de la tarea programada. MIN-> Minuto HOU-> Hora DÍA-> Día MON-> Mes SEMANA-> Días de semana COM-> Común',
    'Periodicity value' => 'Valor de periodicidad',
    'Periodicity description' => 'Descripción de la periodicidad.',
    'It is not possible to create the scheduled task on the server.' => 'No es posible crear la tarea programada en el servidor.'
];
